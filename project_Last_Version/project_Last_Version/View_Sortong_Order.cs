﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class View_Sortong_Order : Form
    {
        public View_Sortong_Order()
        {
            InitializeComponent();
        }
        public void DataBind()
        {
            dataGridView_Costomer.DataSource = null;
            dataGridView_Costomer.DataSource = made_account_userDL.return_list_user();
            dataGridView_Costomer.Refresh();
        }
        private void View_Sortong_Order_Load(object sender, EventArgs e)
        {
            dataGridView_Costomer.DataSource = made_account_userDL.return_list_user();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void cmbSorting_SelectedIndexChanged(object sender, EventArgs e) 
        {
            if(cmbSorting.Text == "Price ")
            {
                List<made_account_user> one = new List<made_account_user>();
                one = made_account_userDL.return_list_user();
                one = one.OrderBy(o => o.Amount_all).ToList();
                dataGridView_Costomer.DataSource = null;
                dataGridView_Costomer.DataSource = one;
                dataGridView_Costomer.Refresh();
            }
            else if(cmbSorting.Text == "CNIC")
            {
                List<made_account_user> one = new List<made_account_user>();
                one = made_account_userDL.return_list_user();
                one = one.OrderByDescending(o => o.Id_card_number).ToList();
                dataGridView_Costomer.DataSource = null;
                dataGridView_Costomer.DataSource = one;
                dataGridView_Costomer.Refresh();
            }
            else if(cmbSorting.Text == "loan")
            {
                List<made_account_user> one = new List<made_account_user>();
                one = made_account_userDL.return_list_user();
                one = one.OrderBy(o => o.Loan).ToList();
                dataGridView_Costomer.DataSource = null;
                dataGridView_Costomer.DataSource = one;
                dataGridView_Costomer.Refresh();
            }
            else if (cmbSorting.Text == "Name") 
            {
                List<made_account_user> one = new List<made_account_user>();
                one = made_account_userDL.return_list_user();
                one = one.OrderBy(o => o.Name).ToList();
                dataGridView_Costomer.DataSource = null;
                dataGridView_Costomer.DataSource = one;
                dataGridView_Costomer.Refresh();
            }
        } 
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_Menu_Fome my = new Admin_Menu_Fome();
            my.ShowDialog();
        }

        private void dataGridView_Costomer_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
