﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Employee_Login : Form
    {
        public Employee_Login()
        {
            InitializeComponent();
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2_Login_Page my = new Form2_Login_Page();
            my.ShowDialog();
        }
        private void btn_login_Click(object sender, EventArgs e)
        {
            if (textBox_Name.Text != "" && textBox_Password.Text != "")
            {
                if(Employee_accountDL.check_valid_Emplyee(textBox_Name.Text , textBox_Password.Text))
                {
                    this.Hide();
                    Form_Employee_Menu my = new Form_Employee_Menu(textBox_Name.Text, textBox_Password.Text);
                    my.ShowDialog();
                } 
                else
                {
                    MessageBox.Show("Invalid User ");
                }
            }
            else
            {
                MessageBox.Show("Something Is Messing  ");
            }

        }

        private void checkBox_pAssword_CheckedChanged(object sender, EventArgs e)
        {
            if(textBox_Password.UseSystemPasswordChar == false )
            {
                textBox_Password.UseSystemPasswordChar = true;
            }
            else
            {
                textBox_Password.UseSystemPasswordChar = false;
            }
        }
    }
}
