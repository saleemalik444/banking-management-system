﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_Last_Version.BL
{
    class transection
    {
        private int withdraw;
        private int send;
        private int Deposit;
        private int _received;

        public int Withdraw { get => withdraw; set => withdraw = value; }
        public int Send { get => send; set => send = value; }
        public int Deposit1 { get => Deposit; set => Deposit = value; }
        public int Received { get => _received; set => _received = value; }

        public transection(int withdraw,  int send, int Deposit)
        {
            this.Deposit = Deposit;
            this.Withdraw = withdraw;
            this.send = send;
        }

        public transection()
        {
            this.Withdraw = 0;
            this.send = 0;
            this.Deposit = 0;
            this._received = 0;
        }
    }
}
