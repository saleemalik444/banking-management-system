﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_Last_Version.BL
{
    class made_account_user
    {
        private string name;
        private string id_card_number;
        private string pass_word;
        private int  Balance;
        private int loan;
        private List<string> feedback = new List<string>();
        private List<transection> transection_one_user = new List<transection>();

        public int Amount_all { get => Balance; set => Balance = value; }
        public string Pass_word { get => pass_word; set => pass_word = value; }
        public string Id_card_number { get => id_card_number; set => id_card_number = value; }
        public string Name { get => name; set => name = value; }
        public int Loan { get => loan; set => loan = value; }

        //______________________________________setter funtion________________________________________
        public void set_list_FeedBack(List<string> feedback)
        {
            this.feedback = feedback;
        }
        public void set_feedback(string feed_back)
        {
            this.feedback.Add(feed_back);
        }
       
        public void set_Transection_List(List<transection> transection_one_user)
        {
            this.transection_one_user = transection_one_user;
        }
        public void make_Loan_Zero(int loan)
        {
            this.loan = this.loan - loan;
        }
        //______________________________________ getter functions ________________________________________
        public List<string> get_Feedback()
        {
            return this.feedback;
        }
        public List<transection> Get_Transection_List()
        {
            return this.transection_one_user;
        }
        public made_account_user(string name,string id_card_number, string pass_word,  int amount_all)
        {
            this.Amount_all = amount_all;
            this.pass_word = pass_word;
            this.name = name;
            this.id_card_number = id_card_number;
        }
        public made_account_user(made_account_user one)
        {
            this.Amount_all = one.Amount_all;
            this.name = one.name;
            this.pass_word = one.pass_word;
            this.id_card_number = one.id_card_number;
        }
        public made_account_user()
        {

        }
        public made_account_user(made_account_user one, transection transection_one_user)
        {
            this.Amount_all = one.Amount_all;
            this.name = one.name;
            this.pass_word = one.pass_word;
            this.id_card_number = one.id_card_number;
            this.transection_one_user.Add(transection_one_user);
        }
        public void add_transection(transection one_transection)
        {
            this.transection_one_user.Add(one_transection);
        }
        public bool send_amount(made_account_user account, int amount_all)
        {
            if (this.Amount_all >= amount_all && amount_all > 0)
            {
                this.Amount_all = this.Amount_all - amount_all;
                account.Amount_all = account.Amount_all + amount_all;
                return true;
            }
            return false;
        }
        public bool withdarw(int amount_all)
        {
            if (this.Amount_all > amount_all) 
            {
                this.Amount_all = this.Amount_all - amount_all;
                return true;
            }
            return false;
        }
        public void Add_feedback(string feedback)
        {
            this.feedback.Add(feedback);
        }

        public int check_Received_Amount()
        {
            int r = 0;
            foreach (var one in transection_one_user)
            {
                r = r + one.Received;
            }
            return r;
        }

    }
}
