﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.DL;
using project_Last_Version.BL;
namespace project_Last_Version
{
    public partial class Form_Employee_Menu : Form
    {
        string name, password;
        public Form_Employee_Menu(string name, string password )
        {
            InitializeComponent();
            this.name = name;
            this.password = password;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Loan_Request my = new Loan_Request(name, password);
            my.ShowDialog();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Profile_E my = new Profile_E(name, password);
            my.ShowDialog();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2_Login_Page my = new Form2_Login_Page();
            my.ShowDialog();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Total_money my = new Total_money(name, password);
            my.ShowDialog();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Employee_Feedback my = new Employee_Feedback(name, password);
            my.ShowDialog();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            give_loan_foam my = new give_loan_foam(name, password);
            my.ShowDialog();
        }
        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }
        private void Form_Employee_Menu_Load(object sender, EventArgs e)
        {

        } 


    }
}
