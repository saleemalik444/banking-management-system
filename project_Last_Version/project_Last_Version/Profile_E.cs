﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;

namespace project_Last_Version
{
    public partial class Profile_E : Form
    {
        string name, password;
        public Profile_E(string name , string password )
        {
            InitializeComponent();
            this.name = name;
            this.password = password;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Employee_Menu my = new Form_Employee_Menu(name, password);
            my.ShowDialog();
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox_CNIC_TextChanged(object sender, EventArgs e)
        {

        }

        private void Profile_E_Load(object sender, EventArgs e)
        {
            Employee_account one =   Employee_accountDL.check_Emplyee_password_Index(password);
            if (one != null)
            {
                textBox_CNIC.Text = one.Employee_ID1;
                textBox_Name.Text = one.Emplyee_name;
                textBox_Password.Text = one.Employee_Password1;
            } 
        }
    }
}
