﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using   project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Form_View_Costomer : Form
    {
        public Form_View_Costomer()
        {
            InitializeComponent();
        }
        public void DataBind() 
        {
            dataGridView_Costomer.DataSource = null;
            dataGridView_Costomer.DataSource = made_account_userDL.return_list_user();
            dataGridView_Costomer.Refresh();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
              dataGridView_Costomer.DataSource = made_account_userDL.return_list_user();
        } 
        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_Menu_Fome my = new Admin_Menu_Fome();
            my.ShowDialog();
        }
        private void dataGridView_Costomer_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            made_account_user one = (made_account_user)dataGridView_Costomer.CurrentRow.DataBoundItem;
            if(dataGridView_Costomer.Columns["Delete"].Index == e.ColumnIndex)
            {
                made_account_userDL.re_move_Costomer(one);
                MessageBox.Show("Costomer is Deleted ");
                DataBind();
            }
            else if (dataGridView_Costomer.Columns["Edit"].Index == e.ColumnIndex)
            {
                Updated_Information my = new Updated_Information(one.Name , one.Pass_word , one.Id_card_number , one.Amount_all);
                this.Hide();
                my.ShowDialog();
                DataBind();

            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
