﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Apply : Form
    {
        string name, password;
        public Apply(string name, string password) 
        {
            InitializeComponent();
            this.name = name;
            this.password = password;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Already Open ");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            pay_loan my = new pay_loan(name, password);
            my.ShowDialog();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if(textBox_Amount.Text  != "" && textBox_password.Text  != "")
            {
                if(made_account_userDL.check_present(textBox_password.Text))
                {
                    if(loan_infoDL.check_Present_loan_password(textBox_password.Text))
                    {
                        MessageBox.Show("Your Already Apply For Loan");
                    }
                    else
                    {
                        loan_info one = new loan_info(textBox_password.Text,int.Parse( textBox_Amount.Text));
                        loan_infoDL.add_loan_info_in_list(one);
                        MessageBox.Show("Your Request For Loan Is Received ");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Password ");
                }
            }
            else
            {
                MessageBox.Show("Something Is Messing ");
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(textBox_password.UseSystemPasswordChar == false)
            {
                textBox_password.UseSystemPasswordChar = true;
            }
            else
            {
                textBox_password.UseSystemPasswordChar = false;
            }
        } 

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Costomer_mENU my = new Costomer_mENU(name, password);
            my.ShowDialog();

        }
    }
}
