﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Total_money : Form
    {
        string name, password;
        public Total_money(string name, string password )
        {
            InitializeComponent();
             this.name = name;
            this.password = password;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int amount = made_account_userDL.all_amount();
            textBox_Money.Text = Convert.ToString(amount);
            if(amount<10000)
            {
                label3.Visible = true;
            }
            else
            {
                label3.Visible = false;
            } 
        }
        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e) 
        {
            this.Hide();
            Form_Employee_Menu my = new Form_Employee_Menu(name, password);
            my.ShowDialog();
        }
    }

}
