﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.DL;
using project_Last_Version.BL;
namespace project_Last_Version
{
    public partial class Updated_Information : Form
    {
        made_account_user obj = new made_account_user();
        public Updated_Information(string name , string password ,  string cnic , int amount )  
        {
            InitializeComponent();
            textBox_Amount.Text = Convert.ToString(amount);
            textBox_Name.Text = name;
            textBox_Password.Text = password;
            textBox_CNIC.Text = cnic;
           
        }  
        private void button_Add_User_Click(object sender, EventArgs e)
        {
            if(textBox_Amount.Text !="" &&  textBox_CNIC.Text !="" && textBox_Name.Text !="" && textBox_Password.Text != "")
            {
                if(!made_account_userDL.check_present(textBox_Password.Text) && !made_account_userDL.check_Present_Cnic(textBox_CNIC.Text))
                {
                      made_account_user one = new made_account_user(textBox_Name.Text ,textBox_CNIC.Text ,textBox_Password.Text  , int.Parse(textBox_Amount.Text));
                    if (made_account_userDL.update(obj, one))
                    {
                        MessageBox.Show("Costomer Information IS Updated ");
                    }
                    
                     this.Hide();
                    Form_View_Costomer my = new Form_View_Costomer();
                    my.ShowDialog();
                } 
                else 
                {
                    MessageBox.Show("This User Is Already Present ");
                }
            } 
            else
            {
                MessageBox.Show("Something Is Messing ");
            }

        }

        private void Updated_Information_Load(object sender, EventArgs e)
        {
            obj.Name = textBox_Name.Text;
            obj.Pass_word = textBox_Password.Text;
            obj.Id_card_number = textBox_CNIC.Text;
            obj.Amount_all = int.Parse(textBox_Amount.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_View_Costomer my = new Form_View_Costomer();
            my.ShowDialog();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
