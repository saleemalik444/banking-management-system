﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using project_Last_Version.BL;
using System.IO;
namespace project_Last_Version.DL
{
    class loan_infoDL
    {

        public static List<loan_info> loan_information = new List<loan_info>();
        public static List<string> password_Give_loan = new List<string>();
        public static void add_Password_Give_loan(string password)
        {
            password_Give_loan.Add(password);
        } 
        public static void add_loan_info_in_list(loan_info one)
        {
            loan_information.Add(one);
        }
        public static bool check_Present_loan_password(string password)
        {
            foreach (var one_pass in loan_information)
            {
                if (one_pass.Password == password)
                {
                    return true;
                }
            }
            return false;
        }
        public static bool check_give_loan(string password)
        {
            foreach (var one in password_Give_loan)
            {
                if (one == password)
                {
                    return true;
                }
            }
            return false;
        }
        public static loan_info check_present_loan_index(string password)
        {
            foreach (var p in loan_information)
            {
                if (p.Password == password)
                {
                    return p;
                }
            }
            return null;
        }
        public static void store_loan_info(string path)
        {
            StreamWriter new_file = new StreamWriter(path, false);

            foreach (loan_info one_one in loan_information)
            {
                new_file.WriteLine(one_one.Amount + "," + one_one.Password);
                new_file.Flush();
            }
            new_file.Close();
        }
        public static void store_loan_password(string path)
        {
            StreamWriter new_file = new StreamWriter(path, false);
            foreach (string one in password_Give_loan)
            {
                new_file.WriteLine(one + ",");
                new_file.Flush();
            }
            new_file.Close();
        }
        public static void load_loan_info(string path)
        {
            string line;
            if (File.Exists(path))
            {
                StreamReader new_file = new StreamReader(path);
                while ((line = new_file.ReadLine()) != null)
                {
                    string[] linesplit = line.Split(',');

                    loan_info one = new loan_info();
                    one.Amount  = (int.Parse(linesplit[0]));
                    one.Password  = (linesplit[1]);
                    loan_information.Add(one);
                }
                new_file.Close();
            }
        }
        public static void load_loan_password(string path)
        {
            string line;
            if (File.Exists(path))
            {
                StreamReader new_file = new StreamReader(path);
                while ((line = new_file.ReadLine()) != null)
                {
                    string[] l = line.Split(',');
                    Console.WriteLine("password  = " + l[0]);
                    password_Give_loan.Add(l[0]);
                }
                new_file.Close();
            }
        }





    }

}
