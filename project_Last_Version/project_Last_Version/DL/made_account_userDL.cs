﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using project_Last_Version.BL;
using System.IO;
namespace project_Last_Version.DL
{
    class made_account_userDL
    {

        public static List<made_account_user> user_account = new List<made_account_user>();
        public static List<made_account_user> return_list_user()
        {
            return user_account;
        }
        public static void add_user(made_account_user one_user)
        {
            user_account.Add(one_user);
        }
        public static bool check_present(string password) 
        {
            foreach (var one_user in user_account)
            {
                if (one_user.Pass_word == password)
                {
                    return true;
                }
            }
            return false;
        }
        public static void re_move_Costomer(made_account_user one)
        {
            user_account.Remove(one);
        }
        public static made_account_user present_password_index(string password) 
        {
            for (int i = 0; i < user_account.Count; i++)
            {
                if (user_account[i].Pass_word == password)
                {
                    return user_account[i];
                }
            }
            return null;
        }
        public static bool check_Present_Cnic(string cnic)
        {
            foreach (var one_user in user_account)
            {
                if (one_user.Id_card_number == cnic)
                {
                    return true;
                }
            }
            return false;
        }
        public static made_account_user check_Present_Cnic_Index(string cnic)
        {
            foreach (var one_user in user_account)
            {
                if (one_user.Id_card_number == cnic)
                {
                    return one_user;
                }
            }
            return null;
        }
        public static bool check_valid_user(string name, string password)
        {
            foreach (made_account_user account in user_account)
            {
                if (account.Pass_word == password && account.Name == name)
                {
                    return true;
                }
            }
            return false;
        }
        public static void store_all_admin_data(string path, string path2, string path3)
        {
            StreamWriter filevariable = new StreamWriter(path, false);
            StreamWriter newfile = new StreamWriter(path2, false);
            StreamWriter feed_file = new StreamWriter(path3, false);
            foreach (made_account_user one in user_account)
            {
                filevariable.WriteLine(one.Pass_word+ "," + one.Name + "," + one.Amount_all + "," + one.Id_card_number + "," + one.Loan);
                for (int i = 0; i < one.Get_Transection_List().Count; i++)
                {
                    newfile.WriteLine(one.Id_card_number + "," + one.Get_Transection_List()[i].Deposit1 + "," + one.Get_Transection_List()[i].Send + "," + one.Get_Transection_List()[i].Withdraw + "," + one.Get_Transection_List()[i].Received + ",");
                }
                for (int i = 0; i < one.get_Feedback().Count; i++)
                {
                    feed_file.WriteLine(one.Id_card_number + "," + one.get_Feedback()[i]);
                }
                filevariable.Flush();
                newfile.Flush();
                feed_file.Flush();
            }
            filevariable.Close();
            newfile.Close();
            feed_file.Close();
        }
        public static void load_account(string path, string path2, string path3)
        {

            string line;
            if (File.Exists(path))
            {
                StreamReader filevariable = new StreamReader(path);
                while ((line = filevariable.ReadLine()) != null)
                {
                    made_account_user one_Account = new made_account_user();
                    string[] parse_line = line.Split(',');
                    one_Account.Pass_word = parse_line[0] ;
                    one_Account.Name = parse_line[1];
                    one_Account.Amount_all = int.Parse(parse_line[2]);
                    one_Account.Id_card_number = parse_line[3];
                    one_Account.Loan = int.Parse(parse_line[4]);
                    one_Account.set_Transection_List(TransectionDL.load_transection(path2, one_Account.Id_card_number));
                    one_Account.set_list_FeedBack(load_feedback(path3, one_Account.Id_card_number));
                    user_account.Add(one_Account);
                }
                filevariable.Close();
            }
        }
        public static List<string> load_feedback(string path, string Cnic)
        {

            string line;
            if (File.Exists(path))
            {
                StreamReader filevariable = new StreamReader(path);
                List<string> feed = new List<string>();
                while ((line = filevariable.ReadLine()) != null)
                {
                    string[] oneline = line.Split(',');
                    if (oneline[0] == Cnic)
                    {
                        string one_feed;
                        one_feed = oneline[1];
                        feed.Add(one_feed);
                    }
                }
                filevariable.Close();
                return feed;
            }
            return null;
        }
        public static   bool  update( made_account_user privious, made_account_user one_new) 
        {
            foreach(var  one in user_account)
            {
                if(one.Name == privious.Name  && one.Pass_word == privious.Pass_word && one.Id_card_number == privious.Id_card_number)
                {
                   
                    one.Name = one_new.Name;
                    one.Pass_word = one_new.Pass_word;
                    one.Id_card_number = one_new.Id_card_number;
                    one.Amount_all = one_new.Amount_all;
                    one.Loan = one_new.Loan;
                    return true;
                } 
            } 
                return false;
        }
        public  static int all_amount()
        {
            int sum = 0;
            foreach(var one in user_account)
            {
                sum = one.Amount_all + sum;
            }
            return sum;
        } 
    }


}
