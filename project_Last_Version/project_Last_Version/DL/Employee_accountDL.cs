﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using project_Last_Version.BL;
using System.IO;
namespace project_Last_Version.DL
{
    class Employee_accountDL
    {
        public static List<Employee_account> new_employee_Accounts = new List<Employee_account>();
        public static List<Employee_account> return_List__employee_Accounts()
        {
            return new_employee_Accounts; 
        }
        public static void add_empyee(Employee_account one_Employee_account)
        {
            new_employee_Accounts.Add(one_Employee_account);
        }
        public static bool check_one_Employee_account(string password)
        {
            foreach (var one_Employee_account in new_employee_Accounts)
            {
                if (one_Employee_account.Employee_Password1 == password)
                {
                    return true;
                }
            }
            return false;
        }
        public static bool check_Emplyee_Id(string id) 
        {
            foreach (Employee_account one in new_employee_Accounts)
            {
                if (one.Employee_ID1 == id)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool check_present_password( string password)
        {
            foreach(var one  in  new_employee_Accounts)
            {
                if(one.Employee_Password1 == password)
                {
                    return true;
                }
            }
            return false;
        }
        public static Employee_account check_Emplyee_password_Index(string password)
        {
            foreach (Employee_account one in new_employee_Accounts)
            {
                if (one.Employee_Password1 == password) 
                {
                    return one;
                } 
            } 
            return null; 
        }
        public static bool check_valid_Emplyee(string name, string password)
        {
            foreach (Employee_account account in new_employee_Accounts)
            {
                if (account.Emplyee_name == name && account.Employee_Password1 == password)
                {
                    return true;
                }
            }
            return false;
        }
        public static void store_Empyee(string path, string path2)
        {
            StreamWriter new_file = new StreamWriter(path, false);
            foreach (Employee_account one in new_employee_Accounts)
            {
                new_file.WriteLine(one.Emplyee_name + "," + one.Employee_Password1 + "," + one.Employee_ID1+ ","  + one.Feed_back) ;
                new_file.Flush();
            }
            new_file.Close();
        }
        public static void load_Empyee(string path, string path2)
        {
            string line;
            if (File.Exists(path))
            {
                StreamReader filevariable = new StreamReader(path);
                while ((line = filevariable.ReadLine()) != null)
                {
                    string[] lineSplit = line.Split(',');
                    Employee_account one = new Employee_account();
                    one.Emplyee_name =  lineSplit[0];
                    one.Employee_Password1 = lineSplit[1];
                    one.Employee_ID1 = lineSplit[2];
                    one.Feed_back  = lineSplit[3];
                    new_employee_Accounts.Add(one);
                }
                filevariable.Close();
            }
        }
     


    }
}
