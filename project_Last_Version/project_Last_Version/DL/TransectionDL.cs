﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using project_Last_Version.BL;
using System.IO;
namespace project_Last_Version.DL
{
    class TransectionDL
    {

        public static List<transection> list_Transection = new List<transection>();
        public static List<transection> load_transection(string path, string cinc)
        {
            StreamReader filevariable = new StreamReader(path);
            string line;
            if (File.Exists(path))
            {
                while ((line = filevariable.ReadLine()) != null)
                {
                    string[] split_Line = line.Split(',');

                    if (split_Line[0] == cinc)
                    {
                        transection one_T = new transection();
                        one_T.Deposit1 = int.Parse(split_Line[1]);
                        one_T.Send = int.Parse(split_Line[2]);
                        one_T.Withdraw = (int.Parse(split_Line[3]));
                        one_T.Received = int.Parse(split_Line[4]);
                        list_Transection.Add(one_T);
                    }
                }
                filevariable.Close();
                return list_Transection;
            }
            return null;
        }




    }

}
