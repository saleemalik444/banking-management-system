﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class View_Feed_Back : Form
    {
        public View_Feed_Back()
        {
            InitializeComponent();
        } 
        public void DataBind()
        {
            dataGridView_Costomer.DataSource = null;
            dataGridView_Costomer.DataSource = Employee_accountDL.return_List__employee_Accounts();
            dataGridView_Costomer.Refresh();
        }
        private void dataGridView_Costomer_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_Menu_Fome my = new Admin_Menu_Fome();
            my.ShowDialog();
        }

        private void View_Feed_Back_Load(object sender, EventArgs e)
        {
            dataGridView_Costomer.DataSource = Employee_accountDL.return_List__employee_Accounts();
        }
    }
}
