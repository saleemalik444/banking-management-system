﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Check_User_Information : Form
    {
        string password; 
        public Check_User_Information(string password )
        {
            InitializeComponent();
            this.password = password;
        } 
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(textBox1.UseSystemPasswordChar == true)
            {
                textBox1.UseSystemPasswordChar = false;
            }
            else
            {
                textBox1.UseSystemPasswordChar = true;
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (made_account_userDL.check_present(textBox1.Text))
            {
                made_account_user one = made_account_userDL.present_password_index(textBox1.Text);
                this.Hide();
                View_Profile my = new View_Profile(one.Name, one.Pass_word, one.Id_card_number, one.Amount_all, one.Loan);
                my.ShowDialog();
            } 
            else
            {
                MessageBox.Show("InValid Password/ Please Try Again ");
            }
        }
        private void btn_Back_Click(object sender, EventArgs e)
        {
            this.Hide();
     ///       Costomer_mENU my = new Costomer_mENU();
         //   my.ShowDialog();
        }

    }
}
