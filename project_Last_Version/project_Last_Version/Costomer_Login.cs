﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Form_Costomer_LOgin : Form
    {
        public Form_Costomer_LOgin()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void btn_login_Click(object sender, EventArgs e)
        {
            if (textBox_Name.Text != "" && textBox_Password.Text != "")
            {
                string name = textBox_Name.Text;
                string passwrow = textBox_Password.Text;
                if (made_account_userDL.check_valid_user(name, passwrow))
                {
                    this.Hide();
                    Costomer_mENU my = new Costomer_mENU(name , passwrow);

                    my.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Invalid user ");
                }
            }
            else
            { 
                MessageBox.Show("Something Is Messing ");
            }
        } 
        private void checkBox_pAssword_CheckedChanged(object sender, EventArgs e)
        {
            if(textBox_Password.UseSystemPasswordChar == false)
            {
                textBox_Password.UseSystemPasswordChar = true;
            }
            else
            {
                textBox_Password.UseSystemPasswordChar = false;
            }
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2_Login_Page my = new Form2_Login_Page();
            my.ShowDialog();

        }
    }
}
