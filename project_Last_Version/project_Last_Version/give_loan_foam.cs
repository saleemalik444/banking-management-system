﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class give_loan_foam : Form
    {
        string name, password;
        public give_loan_foam(string name, string password)
        {
            InitializeComponent();
            this.name = name;
            this.password = password;
        }
        private void button_Give_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != "")
            {
                if(loan_infoDL.check_Present_loan_password(textBox1.Text))
                {
                   loan_info one =   loan_infoDL.check_present_loan_index(textBox1.Text);
                   made_account_user one_user = made_account_userDL.present_password_index(textBox1.Text);
                   one_user.Amount_all = one_user.Amount_all + one.Amount;
                   one_user.Loan = one.Amount;
                   loan_infoDL.add_Password_Give_loan(textBox1.Text);
                   MessageBox.Show("You Give loan to this Account ");
                   textBox1.Text = "";
                } 
                else
                {
                    label_message.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Something Is Messing ");
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            label_message.Visible = false;
        }
        private void give_loan_foam_Load(object sender, EventArgs e)
        {

        }
        private void button_Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Employee_Menu my = new Form_Employee_Menu(name, password);
            my.ShowDialog();
                    
        }


    }
}
