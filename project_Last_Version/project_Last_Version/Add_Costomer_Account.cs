﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using project_Last_Version.BL;
using project_Last_Version.DL;
namespace project_Last_Version
{
    public partial class Add_Costomer_Account : Form
    {
        public Add_Costomer_Account() 
        {
            InitializeComponent();
        }
        public Add_Costomer_Account(string name, string password ,string cnic , int Amount)
        {
            textBox_Name.Text = name;
            textBox_Password.Text = password;
            textBox_CNIC.Text = cnic;
            textBox_Amount.Text  = Convert.ToString(Amount);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_Add_User_Click(object sender, EventArgs e)
        {
            if (textBox_Name.Text != "" && textBox_Password.Text != "" && textBox_Password.Text != "" && textBox_CNIC.Text != "" && textBox_Amount.Text != "")
            {
                if (!made_account_userDL.check_present(textBox_Password.Text) && !made_account_userDL.check_Present_Cnic(textBox_CNIC.Text) ) 
                 {
                    string name = textBox_Name.Text;
                    string password = textBox_Password.Text;
                    string cnic = textBox_CNIC.Text;
                    int amount = int.Parse(textBox_Amount.Text);
                    made_account_user new_one_ = new made_account_user(name, cnic, password, amount);
                    made_account_userDL.add_user(new_one_);
                    MessageBox.Show("New Costomer Is Added");
                }
                else
                {
                    MessageBox.Show("This Costomer Is Already Present ");
                }
            }
            else
            {
                MessageBox.Show("Something  Is Messing ");
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_Menu_Fome my = new Admin_Menu_Fome();
            my.ShowDialog();
           
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if(textBox_Password.UseSystemPasswordChar == false )
            {
                textBox_Password.UseSystemPasswordChar = true;
            }
            else
            {
                textBox_Password.UseSystemPasswordChar = false; 
            }
        }
    }
}
